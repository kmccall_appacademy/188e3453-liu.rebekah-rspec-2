def reverser
  yield.split.map(&:reverse).join(" ")
end

def adder(add_by = 1)
  yield + add_by
end

def repeater(repeat = 1)
  repeat.times do
    yield
  end
end
